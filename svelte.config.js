import adapter from '@sveltejs/adapter-auto';
import { vitePreprocess } from '@sveltejs/kit/vite';
import { VitePWA } from 'vite-plugin-pwa';

const config = {
	plugins: [
		VitePWA({
			includeAssets: ['vite.svg'],
			manifest: {
				name: 'Base',
				short_name: 'Base',
				theme_color: '#0050B3',
				icons: [
					{
						src: '/vite.svg',
						sizes: '192x192',
						type: 'image/png'
					},
					{
						src: '/vite.svg',
						sizes: '512x512',
						type: 'image/png'
					}
				]
			}
		})
	],
	preprocess: vitePreprocess(),
	vitePlugin: {
		inspector: true
	},
	kit: {
		adapter: adapter()
	}
};

export default config;
