export const images = [
    {
        alt: 'iot-1',
        src: 'https://slidebazaar.com/wp-content/uploads/2023/02/Features-of-IoT-slide.jpg',
        title: 'cosmic-timetraveler-pYyOZ8q7AII-unsplash.com'
    },
    {
        alt: 'iot-2',
        src: 'https://slidebazaar.com/wp-content/uploads/2023/02/IOT-implimentation-slides.jpg',
        title: 'cristina-gottardi-CSpjU6hYo_0-unsplash.com'
    },
    {
        alt: 'iot-3',
        src: 'https://slidebazaar.com/wp-content/uploads/2023/02/IoT-evolution-slide.jpg',
        title: 'johannes-plenio-RwHv7LgeC7s-unsplash.com'
    }
];