import { redirect } from '@sveltejs/kit';

const API_URL = 'http://localhost:8080/v1';

export const load = async ({ cookies, fetch }) => {
	const sessionId = cookies.get('Authorization');

	if (!sessionId) {
		throw redirect(301, '/login');
	}
	const res = await fetch(`${API_URL}/bo/users`, {
		headers: {
			Authorization: `Bearer ${sessionId}`
		}
	});
	if (!res.ok) {
		throw new Error(res.statusText);
	}

	/* const respBody = (await res.json()) as { */
	/* 	todos: { */
	/* 		completed: boolean; */
	/* 		description: string; */
	/* 		title: string; */
	/* 		id: number; */
	/* 	}[]; */
	/* }; */

	const body = await res.json();
	console.log('respBody: ', JSON.stringify(body));
	return { body };
};

export const actions = {
	delete: async ({ request, fetch, cookies }) => {
		const formData = await request.formData();
		const id = formData.get('id') || '';

		const sessionId = cookies.get('Authorization');
		if (!sessionId) {
			throw redirect(301, '/login');
		}
		const res = await fetch(`${API_URL}/bo/users/${id}`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${sessionId}`
			}
		});
		if (!res.ok) {
			throw new Error(res.statusText);
		}
		return { success: true };
	},
	updateStatus: async ({ request, fetch, cookies }) => {
		const formData = await request.formData();
		const id = formData.get('id') || '';
		const sessionId = cookies.get('Authorization');
		if (!sessionId) {
			throw redirect(301, '/login');
		}

		const res = await fetch(`${API_URL}/bo/users/${id}/status`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${sessionId}`
			}
		});

		if (!res.ok) {
			throw new Error(res.statusText);
		}

		return { success: true };
	},
	create: async ({ request, fetch, cookies }) => {
		const formData = await request.formData();
		const title = formData.get('title') || '';
		const description = formData.get('description') || '';
		const body = JSON.stringify({ title, description });

		const sessionId = cookies.get('Authorization');
		if (!sessionId) {
			throw redirect(301, '/login');
		}

		const res = await fetch(`${API_URL}/bo/users`, {
			method: 'POST',
			body,
			headers: {
				'content-type': 'application/json',
				Authorization: `Bearer ${sessionId}`
			}
		});

		if (!res.ok) {
			throw new Error(res.statusText);
		}
		return { success: true };
	}
};
