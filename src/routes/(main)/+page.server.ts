import { redirect } from '@sveltejs/kit';
export const load = async (event) => {
	console.log('fetch at home page');
	const accessToken = event.cookies.get('Authorization');
	if (!accessToken) {
		throw redirect(301, '/login');
	}
};
