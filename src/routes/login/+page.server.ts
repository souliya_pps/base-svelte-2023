import { redirect } from '@sveltejs/kit';
import { localStorageStore } from '@skeletonlabs/skeleton';
import type { Writable } from 'svelte/store';

const API_URL = 'http://localhost:8080/v1';
export const load = async (event) => {
	console.log('fetch at login');
	/* const accesToken = event.cookies.get('Authorization'); */
	/* if (accesToken) { */
	/* 	console.log('login pass redirect to home page'); */
	/* 	throw redirect(301, '/'); */
	/* } */
	/* throw redirect(301, '/login'); */
};

export const actions = {
	default: async (event) => {
		const formData = await event.request.formData();
		const identify = formData.get('identify');
		const password = formData.get('password');
		const body = JSON.stringify({ identify: identify, password: password });

		const res = await fetch(`${API_URL}/bo/login`, {
			body,
			method: 'POST',
			headers: { 'content-type': 'application/json' }
		});

		if (res.status === 200) {
			console.log('res: ', res);
			const sessionId = res.headers.get('Authorization');
			event.cookies.set('Authorization', sessionId?.split('Bearer ')[1] ?? '', {
				path: '/'
			});
			throw redirect(301, '/');
		}

		return {
			error: await res.text()
		};
	}
};
