import { redirect } from "@sveltejs/kit";

var API_URL = "http://localhost:8080/v1";

export const load = async (event) => {
  const sessionId = event.cookies.get("Authorization");

  // sign out on the server
  const res = await fetch(`${API_URL}/bo/logout`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${sessionId}`,
    },
  });

  if (res.ok) {
    event.cookies.set("Authorization", "", {
      path: "/",
    });

    throw redirect(301, "/login");
  } else {
    return new Response(await res.text());
  }
};
